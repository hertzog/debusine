Welcome to Debusine's documentation!
====================================

The documentation is structured by following the `Diátaxis
<https://diataxis.fr/>`_ principles: tutorials and explanation
are mainly useful to discover and learn, howtos and reference are
more useful when you are familiar with debusine already and you
have some specific action to perform or goal to achieve.

If you are new to debusine, you will want to read
:ref:`introduction` first.

.. toctree::
   :caption: Tutorials
   :maxdepth: 2

   tutorials/install-your-first-debusine-instance
   tutorials/getting-started-with-debusine

.. toctree::
   :caption: Explanations
   :maxdepth: 2

   explanation/introduction
   explanation/why
   explanation/concepts

.. todo::

   Add new explanation pages to cover:

   * architecture (server, worker, client)
   * work request scheduling (with work request status in all steps)

.. toctree::
   :caption: How-to guides
   :maxdepth: 2

   howtos/set-up-debusine-client
   howtos/create-an-api-token
   howtos/index-admin
   howtos/contribute

.. toctree::
   :caption: Reference
   :maxdepth: 2

   reference/tasks
   reference/artifacts
   reference/faq
   reference/runtime-environment
   reference/package-repositories
   reference/debusine-admin-cli
   reference/debusine-cli
   reference/index-contributors

.. todo::

   Add new reference pages to cover:

   * debusine-client configuration file
   * debusine-server configuration file
   * debusine-worker configuration file

Indices and tables
==================

* :ref:`todo`
* :ref:`genindex`
* :ref:`modindex`

