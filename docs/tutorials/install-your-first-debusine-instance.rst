.. _tutorial-install-debusine:

====================================
Install your first debusine instance
====================================

.. todo::

   Make strong choices adequate to new persons. Ask to setup a bookworm VM
   and the document the minimal steps to perform in that VM. Use the
   repository from deb.freexian.com. Setup and enable a single worker
   and the first debusine user. Ask the user to connect to the web
   interface with its credentials.

   Then propose to continue with a second tutorial where they can
   experiment with the debusine client interface.
