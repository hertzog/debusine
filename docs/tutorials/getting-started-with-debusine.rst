.. _tutorial-getting-started:

=============================
Getting started with debusine
=============================

In this section, we will schedule a few work requests, analyze their
results, and download the generated artifacts. We will also upload our
own artifacts.

Pre-requisites
--------------

We assume that you have a working debusine instance at hand and that you
have :ref:`configured your debusine client <set-up-debusine-client>`.

If you are a Debian developer, you can use `debusine.debian.net
<https://debusine.debian.net>`, login there with your salsa.debian.org
account, :ref:`create an API token <create-api-token>`,
and record the token in the :ref:`debusine client configuration
<set-up-debusine-client>`.

Otherwise, please consider starting your journey by :ref:`installing
your own debusine instance <tutorial-install-debusine>`.

.. todo::

   The remaining of this page comes straight from the former
   user/debusine-client and it needs to be reworked. We need a much
   simpler sequence of steps without any gory detail yet. The gory
   details needs to be moved in some howtos and/or reference pages.

   I'd suggest to first schedule a "mmdebstrap" task that generates
   an artifact. Then inspect that artifact and download it.

   Then we should use the (upcoming) debian-import command to upload a
   source package and then schedule a sbuild task for this source package.

   We can throw in some screenshots of the corresponding views on the web
   interface. And that's all.

.. _submitting-a-work-request:

Submitting a work request
-------------------------

To submit a work request:

  #. Create a file with the work request, for example:

     .. code-block:: console

      $ cat > work-request-hello.debusine << END
      build_components:
      - any
      - all
      distribution: stable
      host_architecture: amd64
      input:
        source_artifact_id: 5
      END

     It is possible to add sbuild options.
     Note: this is a temporary feature; to avoid arbitrary command execution on the worker,
     we will prevent that in the long term.
     For example, to add ``post-build-commands`` include the ``sbuild_options`` section:

     .. code-block:: console

      $ cat > work-request-hello.debusine << END
      build_components:
      - any
      - all
      distribution: stable
      host_architecture: amd64
      input:
        source_artifact_id: 5
      sbuild_options:
      - --post-build-commands=/bin/cp %SBUILD_CHANGES /tmp/
      END

  #. Submit the work request of type sbuild:

     .. code-block:: console

       $ debusine create-work-request sbuild < work-request-hello.debusine

     The command will show an output such as:

     .. code-block:: yaml

       result: success
       message: Work request registered on http://localhost/api with id 5.
       work_request_id: 5

  #. Check the status of the work request:

     .. code-block:: console

       $ debusine show-work-request 5

  #. If the status shows ``status: pending`` without a ``worker`` it might indicate
     that there is no ``debusine-worker`` available yet. At some point, the
     work request will have a ``worker`` assigned and then the status should be
     ``running``, then ``completed`` or ``aborted``.

     Once the status is ``completed``, the field ``result`` will be ``success``
     or ``failure``.

.. _creating-an-artifact:

Creating an artifact
--------------------

To create an artifact for the category ``Test`` uploading ``FILE1`` and ``FILE2``:

.. code-block:: console

 $ debusine create-artifact Test --workspace System --upload FILE1 FILE2
 result: success
 message: New artifact created in http://localhost:8011/api in workspace System with id 61.
 source_artifact_id: 61
 files_to_upload: 2

The artifact is creating and the file are uploaded automatically.

Check the documentation for the create-artifact regarding uploading multiple
files, files relative to a directory, etc. using:

.. code-block:: console

 $ debusine create-artifact --help


The artifact can be downloaded via:

.. code-block:: console

 $ debusine download-artifact ARTIFACT_ID

.. _creating-work-request-using-an-artifact:

Creating an sbuild work-request using an artifact
-------------------------------------------------

In the example above sbuild downloaded the .dsc file.

In this section debusine will download the artifact and sbuild will use
the .dsc (and other required files) from the artifact.

#. Prepare a directory with the relevant files

   .. code-block:: console

    $ mkdir temp_directory/
    $ cd temp_directory/
    $ dget -d http://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.dsc

#. Create the artifact

   .. code-block:: console

    $ debusine create-artifact Test --workspace System --upload *
    result: success
    message: New artifact created in http://localhost:8011/api in workspace System with id 62.
    artifact_id: 62
    files_to_upload: 3

#. Create the file indicating the artifact_id that is going to be used:

   .. code-block:: console

    $ cat > work-request-hello.debusine << END
    build_components:
    - any
    - all
    distribution: stable
    host_architecture: amd64
    input:
      source_artifact_id: 62
    END

#. Create the work-request

   .. code-block:: console

    $ debusine create-work-request sbuild < work-request-hello.debusine
    result: success
    message: Work request registered on http://localhost:8011/api with id 53.
    work_request_id: 53

#. Check the status:

   .. code-block:: console

    $ debusine show-work-request 53

.. _creating-work-request-with-notification:

Creating a work request with a notification
-------------------------------------------

For the following example to work, the Debusine administrator should
have created a channel notification `lts`. Refer to the
:ref:`creating channel to send emails section <creating-channel-to-send-emails>`.

   .. code-block:: console
    :emphasize-lines: 9-13

    $ debusine create-work-request sbuild << END
    build_components:
    - any
    - all
    distribution: stable
    host_architecture: amd64
    input:
        source_artifact_id: 5
    notifications:
        on_failure:
            - channel: lts
              to: ["lts@example.com"]
              subject: "Work request {work_request_id}: result: {work_request_result}"
    END

In this case, the notification channel default To: email is replaced by
"lts@example.com" (it could contain multiple emails) and the subject
will be "Work request {work_request_id}: result: {work_request_result}".

In the subject, the strings ``{work_request_id}`` and ``{work_request_result}``
are replaced by their values.

The possible fields for email notifications are:

- `from`: a default is provided by the debusine admin
- `to`: a default is provided by the debusine admin
- `subject`: a default is provided by the debusine admin
- `cc`: list of emails
