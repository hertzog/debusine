===================================
Reference for debusine contributors
===================================

.. toctree::

   design-goals
   Team organization <development-team-organization>
   coding-practices
   internal-api/index

