.. _debusine-admin-cli:

==========================
The debusine-admin command
==========================

The ``debusine-admin`` command is the usual `django-admin
<https://docs.djangoproject.com/en/3.2/ref/django-admin/>`_ command
of a Django project. But debusine adds many `custom management commands
<https://docs.djangoproject.com/en/3.2/howto/custom-management-commands/>`_
that are documented on this page.

This command must be executed on the server while connected as the
``debusine-server`` user.

For commands that are currently undocumented, you can still explore their
features by using the ``--help`` command line option::

.. code-block:: console

  $ sudo -u debusine-server debusine-admin edit_worker_metadata --help

.. todo::

   Document all the (remaining) subcommands:
   * changepassword
   * createsuperuser
   * create_notification_channel
   * create_token
   * create_user
   * create_workspace
   * delete_expired_artifacts
   * delete_notification_channel
   * delete_tokens
   * edit_worker_metadata
   * list_notification_channels
   * list_users
   * list_work_requests
   * list_workers
   * list_workspaces
   * manage_notification_channel
   * manage_token
   * manage_user
   * manage_worker
   * manage_workspace
   * monthly_cleanup

Managing workers
----------------

``manager_worker``
~~~~~~~~~~~~~~~~~~

``edit_worker_metadata``
~~~~~~~~~~~~~~~~~~~~~~~~

``list_workers``
~~~~~~~~~~~~~~~~

Managing users
--------------

``create_user``
~~~~~~~~~~~~~~~

``manage_user``
~~~~~~~~~~~~~~~

``changepassword``
~~~~~~~~~~~~~~~~~~

``create_super_user``
~~~~~~~~~~~~~~~~~~~~~

``list_users``
~~~~~~~~~~~~~~

Manage workspaces
-----------------

``create_workspace``
~~~~~~~~~~~~~~~~~~~~

``manage_workspace``
~~~~~~~~~~~~~~~~~~~~

``list_workspaces``
~~~~~~~~~~~~~~~~~~~

Manage work requests
--------------------

``list_work_requests``
~~~~~~~~~~~~~~~~~~~~~~

Manage notification channels
----------------------------

``create_notification_channel``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``manage_notification_channel``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``list_notification_channels``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``delete_notification_channel``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Managing tokens
---------------

``create_token``
~~~~~~~~~~~~~~~~

``manage_token``
~~~~~~~~~~~~~~~~

``list_tokens``
~~~~~~~~~~~~~~~

The command ``list_tokens`` lists all tokens by default. It is possible
to filter tokens by the owner or the token itself, using the options ``--owner``
or ``--token``, for example:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_tokens --owner OWNER
  $ sudo -u debusine-server debusine-admin list_tokens --token TOKEN

The command ``list_workers`` does not have filtering options.

``delete_tokens``
~~~~~~~~~~~~~~~~~

Tokens can be removed using the ``delete_tokens`` command. By default, it asks
for interactive confirmation unless ``--yes`` is used. See the options using:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin delete_tokens --help

Administrative commands
-----------------------

``delete_expired_artifacts``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``monthly_cleanup``
~~~~~~~~~~~~~~~~~~~


