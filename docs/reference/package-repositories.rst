.. _debusine-package-repositories:

====================
Package repositories
====================

You can install debusine from 3 different package repositories.

The Debian repository
---------------------

debusine is available in the Debian archive, currently only in
the experimental repository but soon in sid (unstable) and trixie
(testing) too.

The upstream repository
-----------------------

This repository contains the latest upstream release of debusine and can
be installed in Debian 12 (and Debian 11 with bullseye-backports for some
packages).

.. include:: /common/add-upstream-repository.rst

The snapshot repository
-----------------------

This repository contains packages built out of the ``devel`` branch of
debusine's git repository. It can be used to test the next release that is
still in development.

.. include:: /common/add-snapshot-repository.rst

The bullseye-backports repository
---------------------------------

If you want to run debusine on Debian 11, you will need to enable the
bullseye-backports repository and install python3-django from that
repository:

.. code-block:: console

  # Only needed on Bullseye
  $ sudo tee /etc/apt/sources.list.d/bullseye-backports.list <<END
  deb http://deb.debian.org/debian bullseye-backports main
  END
  $ sudo apt update
  $ sudo apt install python3-django/bullseye-backports
