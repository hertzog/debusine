.. _artifacts:

=========
Artifacts
=========

The categorization of artifacts does not enforce anything on the
structure of the associated files and key-value data. However, there must
be some consistency and rules to be able to a make a meaningful use of the
system.

This document presents the various categories that we use to manage a
Debian-based distribution. For each category, we explain:

* what associated files you can find
* what key-value data you can expect
* what relationships with other artifacts are likely to exist

Category ``debian:source-package``
==================================

This artifact represents a set of files that can be extracted in some
way to provide a file hierarchy containing source code that can be built
into ``debian:binary-packages`` artifact(s).

* Data:

  * name: the name of the source package
  * version: the version of the source package
  * type: the type of the source package

    * ``dpkg`` for a source package that can be extracted with ``dpkg-source -x`` on the ``.dsc`` file

  * dsc-fields: a parsed version of the fields available in the .dsc file

* Files: for the ``dpkg`` type, a ``.dsc`` file and all the files
  referenced in that file
* Relationships: none

Category ``debian:binary-packages``
===================================

This artifact represents the set of binary packages (``.deb`` files and
similar) produced during the build of a source package for a given
architecture.

If the build of a source-package produces binaries of more than one
architecture, one ``debian:binary-packages`` artifact is created for each
architecture, listing only the binary packages for that architecture.

* Data:

  * srcpkg-name: the name of the source package
  * srcpkg-version: the version of the source package
  * version: the version used for the build (can be different from the
    source version in case of binary-only rebuilds)
  * architecture: the architecture that the packages have been built for.
    Can be any real Debian architecture or ``all``.
  * packages: the list of binary packages that are part of the build
    for this architecture.

* Files: one or more ``.deb`` files
* Relationships:

  * built-using: the corresponding ``debian:source-package``
  * built-using: other ``debian:binary-packages`` (for example in the case of
    signed packages duplicating the content of an unsigned package)
  * built-using: other ``debian:source-package`` (general case of Debian's
    ``Built-Using`` field)

Category ``debian:upload``
==========================

This artifact represents an upload of source and/or binary packages.
Currently uploads are always represented with ``.changes`` file but the
structure of the artifact makes it possible to represent other kind of
uploads in the future (like uploads with signed git tags, or some
debusine native internal upload).

* Data:

  * type: the type of the source upload

    * ``dpkg``: for an upload generated out of a ``.changes`` file created
      by ``dpkg-buildpackage``

  * changes-fields: a parsed version of the fields available in the
    ``.changes`` file

* Files:

  * a ``.changes`` file
  * All files mentioned in the ``.changes`` file

* Relationships:

  * extends: (optional) one ``debian:source-package``
  * extends: (optional) one or more ``debian:binary-packages``

Category ``debian:package-build-log``
=====================================

This artifact contains a package's build log and some associated
information about the corresponding package build. It is kept around
for traceability and for diagnostic purposes.

* Data:

  * source: name of the source package built
  * version: version of the source package built
  * filename: name of the log file
  * maybe other information extracted out of the build log (build time,
    disk space used, etc.)

* Files:

  * a single file ``.build`` file

* Relationships:

  * relates-to: one (or more) ``debian:binary-packages`` built
  * relates-to: the corresponding ``debian:source-package`` (if built from a
    source package)

Category ``debian:system-tarball``
==================================

This artifact contains a tarball of a Debian system. The tarball is
compressed with ``xz``.

* Data:

  * filename: filename of the tarball inside the artifact (e.g.
    "system.tar.xz").
  * vendor: name of the distribution vendor (can be found in ID field in
    /etc/os-release)
  * codename: name of the distribution used to bootstrap the system
  * mirror: URL of the mirror used to bootstrap the system
  * variant: value of ``--variant`` parameter of debootstrap
  * pkglist: a dictionary listing versions of installed packages (cf
    ``dpkg-query -W``)
  * architecture: the architecture of the Debian system
  * with_dev: boolean value indicating whether ``/dev`` has been populated
    with the most important special files in ``/dev`` (null, zero, full,
    random, urandom, tty, console, ptmx) as well as some usual symlinks
    (fd, stdin, stdout, stderr).
  * with_init: boolean value indicating whether the system contains an
    init system in ``/sbin/init`` and can thus be "booted" in a container.

* Files:

  * ``$filename`` (e.g. ``system.tar.xz``): tarball of the Debian system

* Relationships:

  * None.

.. note::
   In preparation of support of different compression schemes, we have
   decided that the extension of the filename dictates the compression
   scheme used and that it should be compatible with ``tar
   --auto-compress``.

.. _system-image-artifact:

Category ``debian:system-image``
================================

This artifact contains a disk image of a Debian system. The disk has
a GPT partition table and is EFI bootable (for architectures that support
EFI). The disk image should usually contain an EFI partition and a
partition for the root filesystem. The root partition should use
a partition type UUID respecting the `discoverable partitions
specification
<https://uapi-group.org/specifications/specs/discoverable_partitions_specification/>`__.

* Data:

  * Same as ``debian:system-tarball`` with some extra fields. The
    ``filename`` field points to the disk image.
  * image_format: indicates the format of the image (e.g. ``raw``,
    ``qcow2``)
  * filesystem: indicates the filesystem used on the root filesystem (e.g.
    ``ext4``, ``btrfs``, ``iso9660``)
  * size: indicates the size of the filesystem on the root filesystem (in
    bytes)
  * boot_mechanism: a list of all the ways that the image can be booted.
    Valid values are ``efi`` and ``bios``.

* Files:

  * ``$filename`` (e.g. ``image.tar.xz``, ``image.qcow2``, ``image.iso``):
    the nature of the file depends on the ``image_format`` field.

* Relationships:

  * None.

.. note::
   At this point, we expect official Debusine tasks to only generate and
   use images that are bootable with EFI. But the artifact specification
   has the ``boot_mechanism`` key to be future-proof and for the benefit
   of custom tasks that would make different choices.

``raw`` image format
~~~~~~~~~~~~~~~~~~~~

The image itself is wrapped in a xz-compressed tarball to be able to
properly support sparse filesystem images (i.e. files with holes without
any data) and to save some space with compression.

The ``filename`` field points to the tarball that should contain a
``root.img`` file which is the raw disk image.

``qcow2`` image format
~~~~~~~~~~~~~~~~~~~~~~

The ``filename`` field points directly to the ``qcow2`` image.

Category ``debusine:work-request-debug-logs``
=============================================

* Files:

  * any number of files containing logs and information to help a debusine user
    understand the WorkRequest output: commands executed, output of the
    commands, etc.

* Relationships:

  * relates-to: the corresponding ``debian:source-package`` (if built from a
    source package)
