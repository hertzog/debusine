.. _debusine-cli:

====================
The debusine command
====================

The ``debusine`` command is a command line interface to the debusine API.
It is provided by the ``debusine-client`` package and contains many
sub-commands.

Output of the ``debusine`` command
----------------------------------
If the ``debusine`` command succeeds, it prints relevant information to the
standard output in YAML format.

If an error occurs, the error messages will be printed to the standard error.

Return values
-------------

Return values of the ``debusine`` command:

===============  ==================================================================================
  Return value    Meaning
===============  ==================================================================================
 0                Success
 1                Error: unhandled exception. Please report the error
 2                Error: wrong arguments and options
 3                Error: any other type of error such as connection to the server is not possible,

                  invalid configuration file, etc.
===============  ==================================================================================

Sub-commands
------------

.. todo::

   Document all the subcommands.


