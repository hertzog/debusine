.. code-block:: console

  $ sudo tee /etc/apt/sources.list.d/debusine.list <<END
  deb [trusted=yes] https://deb.freexian.com/packages/debusine/ ./
  END
  $ sudo apt update
