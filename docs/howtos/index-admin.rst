========================
Tasks for administrators
========================

.. toctree::

   set-up-debusine-server
   set-up-debusine-worker
   add-new-worker
   add-new-user
   enable-logins-with-gitlab
