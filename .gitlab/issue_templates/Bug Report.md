### Describe the bug

_Please describe the undesired behaviour._

### How to reproduce the bug

_List all the steps to follow to reproduce the undesired behaviour._

### Runtime environment

#### Operating system

_Document the OS that you used (and its version) when you triggered the
undesired behaviour._

#### Versions of debusine and its dependencies

_Please document the debusine version that you are running, as well as the
version of the various dependencies (cf setup.cfg for a list of
dependencies)._
