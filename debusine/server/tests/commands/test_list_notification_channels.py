# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the management command list_notification_channels."""

from django.test import TestCase

from debusine.db.models import NotificationChannel
from debusine.server.tests.commands import call_command


class ListNotificationChannelsTests(TestCase):
    """Tests for the list_notification_channels command."""

    def setUp(self):
        """Set up new notification channel for the test."""
        data = {
            "from": "sender@debusine",
            "to": ["recipient@example.com"],
        }
        self.notification_channel = NotificationChannel.objects.create(
            name="lts", method=NotificationChannel.Methods.EMAIL, data=data
        )

    def test_list_notification_channels(self):
        """list_notification_channels print the channels."""
        stdout, stderr, exit_code = call_command("list_notification_channels")

        self.assertIn(self.notification_channel.name, stdout)
        self.assertIn(self.notification_channel.method, stdout)
        self.assertIn(self.notification_channel.data["to"][0], stdout)
        self.assertIn(self.notification_channel.data["from"], stdout)
        self.assertIn("Number of notification channels: 1\n", stdout)

        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)
