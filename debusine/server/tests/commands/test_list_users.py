# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the management command list_users."""

from django.contrib.auth import get_user_model
from django.test import TestCase

from debusine.server.management.utils import datetime_to_isoformat
from debusine.server.tests.commands import call_command


class ListUsersCommandTests(TestCase):
    """Tests for the list_users command."""

    def setUp(self):
        """Set up new user for the tests."""
        self.user = get_user_model().objects.create_user(
            username="bob", email="bob@bob.com", password="123456"
        )

    def test_list_tokens_no_filtering(self):
        """list_users print the user's information."""
        stdout, stderr, exit_code = call_command("list_users")

        self.assertIn(self.user.username, stdout)
        self.assertIn(self.user.email, stdout)
        self.assertIn(datetime_to_isoformat(self.user.date_joined), stdout)
        self.assertIn("Number of users: 1\n", stdout)

        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)
