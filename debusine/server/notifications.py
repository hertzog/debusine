# Copyright 2021-2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Notifications using channels or NotificationChannels."""

import logging
import string

from asgiref.sync import async_to_sync

from channels.layers import get_channel_layer

from django.core.mail import EmailMessage

logger = logging.getLogger(__name__)


def notify_work_request_assigned(work_request: "WorkRequest"):  # noqa: F821
    """Send a channel message of type 'work_request_assigned'."""
    if work_request.worker is None:
        return

    async_to_sync(get_channel_layer().group_send)(
        work_request.worker.token.key,
        {"type": "work_request.assigned"},
    )


def notify_worker_token_disabled(token: "Token"):  # noqa: F821
    """Notify to the WorkerConsumer that a worker has been disabled."""
    if not hasattr(token, "worker"):
        return

    async_to_sync(get_channel_layer().group_send)(
        token.key, {"type": "worker.disabled"}
    )


def notify_work_request_completed(work_request: "WorkRequest"):  # noqa: F821
    """Notify to the interested parties that work_request is completed."""
    # Send to the channel_layer (Websockets) that the work request has completed
    async_to_sync(get_channel_layer().group_send)(
        "work_request_completed",
        {
            "type": "work_request_completed",
            "work_request_id": work_request.id,
            "workspace_id": work_request.workspace.id,
            "completed_at": work_request.completed_at.isoformat(),
            "result": work_request.result,
        },
    )

    # Prepare the notifications to the Notification channel based on the
    # WorkRequest "notifications" section in task_data (e.g. email
    # notifications)
    work_request_notifications = work_request.task_data.get("notifications")

    if work_request_notifications is None:
        # WorkRequest didn't have any notifications to be sent
        return

    from debusine.db.models import WorkRequest
    from debusine.db.models import NotificationChannel

    if "on_failure" in work_request_notifications and work_request.result in (
        WorkRequest.Results.FAILURE,
        WorkRequest.Results.ERROR,
    ):
        for work_request_notification in work_request_notifications[
            "on_failure"
        ]:
            name = work_request_notification["channel"]
            try:
                notification_channel = NotificationChannel.objects.get(
                    name=name
                )
            except NotificationChannel.DoesNotExist:
                logger.debug(
                    "WorkRequest %s: on_failure notification cannot be sent: "
                    "NotificationChannel %s does not exist",
                    work_request.id,
                    name,
                )
                continue

            body = "Sent by debusine"

            _send_work_request_completed_notification(
                notification_channel,
                work_request,
                work_request_notification,
                body,
            )


def _send_work_request_completed_notification(
    notification_channel: "NotificationChannel",  # noqa: F821
    work_request: "WorkRequest",  # noqa: F821
    work_request_notification: dict,
    body: str,
):
    from debusine.db.models import NotificationChannel

    if notification_channel.method != NotificationChannel.Methods.EMAIL:
        raise NotImplementedError()

    email_data = {**notification_channel.data, **work_request_notification}

    subject = email_data.get(
        "subject",
        "WorkRequest $work_request_id completed in $work_request_result",
    )
    subject = string.Template(subject).safe_substitute(
        work_request_id=work_request.id, work_request_result=work_request.result
    )

    email = EmailMessage(
        subject=subject,
        body=body,
        from_email=email_data["from"],
        to=email_data["to"],
        cc=email_data.get("cc"),
    )

    success = email.send(fail_silently=True)

    if success == 0:
        logger.warning(
            "NotificationChannel email notification for WorkRequest "
            "%s failed sending",
            work_request.id,
        )
