# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Functions to get system information."""

import psutil


def total_physical_memory():
    """Return bytes of RAM memory in system."""
    return psutil.virtual_memory().total


def cpu_count():
    """Return number of CPUs in the system."""
    return psutil.cpu_count()
