# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Mixins useful for Debusine views."""


class PaginationMixin:
    """Add elided_page_range in "context" via get_context_data()."""

    def get_context_data(self, *args, **kwargs):
        """Return super().get_context_data with elided_page_range."""
        context = super().get_context_data(*args, **kwargs)

        page_obj = context["page_obj"]
        context["elided_page_range"] = page_obj.paginator.get_elided_page_range(
            page_obj.number
        )

        return context
