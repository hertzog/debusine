# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""View for Lintian work request."""
import json
import uuid
from typing import Union

from django.urls import reverse

from debusine.artifacts import LintianArtifact
from debusine.db.models import Artifact, FileInArtifact, WorkRequest
from debusine.tasks import Lintian
from debusine.web.views.work_request import WorkRequestPlugin


class LintianView(WorkRequestPlugin):
    """View for Lintian work request."""

    template_name = "web/lintian-detail.html"
    task_name = "lintian"

    @classmethod
    def _artifacts_to_tags(
        cls, artifacts
    ) -> dict[str, dict[str, Union[str, list]]]:
        """
        Receive a list of artifacts and return dict with file names and tags.

        :param artifacts: each of contains an "analysis.json" file with "tags"
         key, with a list of tags. Each tag is such as:
            {
               "severity": "pedantic", "package": "hello",
               "tag": "license-problem", "explanation": "foo",
               "note": "bar", "pointer": "debian/copyright"
            }
        :return: a dictionary of filenames to dictionary of containing the
           tags grouped. For example:
                    {
                        "hello_2.10-3.dsc": {
                            "license-problem":
                            {
                                "explanation": "the explanation",
                                "pointer": "severity",
                                "uuid": "used_by_the_view",
                                "occurrences": [
                                    {
                                        "explanation": "foo",
                                        "pointer": "debian/copyright"
                                    },
                                ]
                            }
                        }
                    }

        """  # noqa: RST201, RST203, RST301
        # "tags": intermediate data structure: contains each file (.dsc,
        # .deb, etc.) with each tag for the file with all the information
        # (explanation, pointer, comment, etc.).
        tags: dict[str, dict[str, any]] = {}

        # "tags_information": intermediate data structure with each tag_name
        # containing a dictionary with the common data: explanation, pointer
        # and severity
        tags_information: dict[str, dict[str, str]] = {}

        for analysis in cls._get_analyses(artifacts):
            for tag in analysis["tags"]:
                package = tag["package"]
                filename = analysis["summary"]["package_filename"][package]

                tag_name = tag["tag"]
                # Explanation and severity is the same for each instance
                # of this tag (incorporated in tags_with_information).
                tag_contents = {
                    "pointer": tag["pointer"],
                    "note": tag["note"].replace("\\n", "\n"),
                    "comment": tag["comment"],
                }
                tags_information[tag_name] = {
                    "explanation": tag["explanation"],
                    "severity": tag["severity"],
                }

                tags.setdefault(filename, {}).setdefault(tag_name, []).append(
                    tag_contents
                )

        # "tags_with_information" is the final structure as explained
        # in the docstring
        tags_with_information = {}

        for filename, tags_in_filename in tags.items():
            tags_with_information[filename] = {}
            for tag_name, occurrences in tags_in_filename.items():
                tags_with_information[filename][tag_name] = tags_information[
                    tag_name
                ]
                tags_with_information[filename][tag_name]["uuid"] = uuid.uuid4()
                tags_with_information[filename][tag_name][
                    "occurrences"
                ] = occurrences

        return tags_with_information

    @classmethod
    def _get_summaries(cls, artifacts) -> list[dict[str, any]]:
        """Return list of all the artifacts.data["summary"] in artifacts."""
        return [artifact.data["summary"] for artifact in artifacts]

    @staticmethod
    def _get_analyses(artifacts) -> list[dict]:
        """Return a list with all the parsed analysis.json in artifacts."""
        analyses: list[dict] = []

        for artifact in artifacts:
            try:
                analysis_in_artifact = artifact.fileinartifact_set.get(
                    path="analysis.json"
                )
            except FileInArtifact.DoesNotExist:
                continue

            workspace = artifact.workspace
            file_store = workspace.default_file_store.get_backend_object()

            with file_store.get_stream(analysis_in_artifact.file) as file:
                analysis = json.loads(file.read())

            analyses.append(analysis)

        return analyses

    @staticmethod
    def _find_lintian_txt_url_path(artifacts) -> str:
        """
        Return the URL path for the lintian.txt file.

        The path is for any of the lintian.txt found in artifacts.
        """
        for artifact in artifacts:
            for file_in_artifact in artifact.fileinartifact_set.order_by("id"):
                if file_in_artifact.path == Lintian.CAPTURE_OUTPUT_FILENAME:
                    return (
                        reverse(
                            "artifacts:detail",
                            kwargs={"artifact_id": artifact.id},
                        )
                        + Lintian.CAPTURE_OUTPUT_FILENAME
                    )

        return None

    @classmethod
    def _count_by_severity(cls, artifacts) -> dict[str, int]:
        tags_count_severity = Lintian.generate_severity_count_zero()
        tags_count_severity["overridden"] = 0

        for summary in cls._get_summaries(artifacts):
            for severity, count in summary.get(
                "tags_count_by_severity", {}
            ).items():
                tags_count_severity[severity] += count

        return tags_count_severity

    @staticmethod
    def _list_files(artifact_id: int) -> list[str]:
        return [
            file.path
            for file in Artifact.objects.get(
                id=artifact_id
            ).fileinartifact_set.order_by("path")
        ]

    @classmethod
    def _get_request_data(cls, work_request: WorkRequest) -> dict[str, any]:
        """Prepare the requested data to be displayed."""
        request_data = {}

        task_data_input = work_request.task_data["input"]

        if "source_artifact_id" in task_data_input:
            source_artifact_id = task_data_input["source_artifact_id"]

            request_data["source_artifact"] = {
                "id": source_artifact_id,
                "files": cls._list_files(source_artifact_id),
            }

        binary_artifacts = []
        for binary_artifact_id in task_data_input.get(
            "binary_artifacts_ids", []
        ):
            binary_artifacts.append(
                {
                    "id": binary_artifact_id,
                    "files": cls._list_files(binary_artifact_id),
                }
            )

        request_data["binary_artifacts"] = binary_artifacts

        return request_data

    def get_context_data(self, work_request: WorkRequest):
        """Return the context."""
        artifacts = work_request.artifact_set.filter(
            category=LintianArtifact._category
        ).order_by("id")

        lintian_txt_path = self._find_lintian_txt_url_path(artifacts)

        return {
            "request_data": self._get_request_data(work_request),
            "result": work_request.result,
            "lintian_txt_path": lintian_txt_path,
            "fail_on_severity": work_request.task_data.get("fail_on_severity"),
            "tags": self._artifacts_to_tags(artifacts),
            "tags_count_severity": self._count_by_severity(artifacts),
        }
