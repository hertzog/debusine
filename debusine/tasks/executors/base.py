# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Interface for executor backends."""

from abc import ABC, abstractmethod
from pathlib import Path, PurePath
from subprocess import CompletedProcess
from typing import Any

from debusine.client.debusine import Debusine
from debusine.client.models import ArtifactResponse

_backends = {}


class ExecutorInterface(ABC):
    """
    Interface for managing an executor.

    All executors must support this minimal interface.
    """

    system_image: ArtifactResponse

    def __init_subclass__(cls, /, backend_name, **kwargs):
        """Register the backend in _backends."""
        super().__init_subclass__(**kwargs)
        cls.backend_name = backend_name
        _backends[backend_name] = cls

    @abstractmethod
    def __init__(self, debusine_api: Debusine, system_image_id: int):
        """
        Instantiate an ExecutorInterface.

        :param debusine_api: The object to use the debusine client API
        :param system_image_id: An artifact id pointing to the system tarball
            or disk image to use (as appropriate for each technology)
        """

    @classmethod
    @abstractmethod
    def available(cls) -> bool:
        """Determine whether this executor is available for operation."""

    @abstractmethod
    def download_image(self) -> str:
        """
        Make the image available locally.

        Fetch the image from artifact storage, if it isn't already available,
        and make it available locally.

        Return a path to the image or name, as appropriate for the backend.
        """

    @abstractmethod
    def create(self) -> "InstanceInterface":
        """Create / Launch an instance of the image."""

    @abstractmethod
    def autopkgtest_virt_args(self) -> list[str]:
        """Generate the arguments to pass to autopkgtest-virt-$backend."""


class InstanceInterface(ABC):
    """
    Interface for execution within an instance of an execution environment.

    All execution environments support this minimal interface.
    """

    @abstractmethod
    def start(self) -> None:
        """Start the environment, making it ready to run commands."""

    @abstractmethod
    def stop(self) -> None:
        """Stop the environment."""

    @abstractmethod
    def file_push(self, source: Path, target: PurePath) -> None:
        """Copy a file into the environment."""

    @abstractmethod
    def file_pull(self, source: PurePath, target: Path) -> None:
        """Copy a file out of the environment."""

    @abstractmethod
    def run(self, *args: list[str], **kwargs) -> CompletedProcess:
        """
        Run a command in the environment.

        Arguments behave as if passed to `subprocess.run`.
        """


def executor(
    debusine_api: Debusine, backend: str, environment_id: int
) -> InstanceInterface:
    """
    Instantiate the Executor instance.

    :param backend: The name of the executor backend.
    :param environment_id: The Artifact ID of the execution environment.
    :return: An instantiated InstanceInterface.
    """
    if backend not in _backends:
        raise NotImplementedError(
            f"Support for backend {backend} is not yet implemented."
        )

    return _backends[backend](debusine_api, environment_id)


def analyze_worker_all_executors() -> dict[str, Any]:
    """
    Return dictionary with metadata for each executor backend.

    This method is called on the worker to collect information about the
    worker. The information is stored as a set of key-value pairs in a
    dictionary.

    That information is then reused on the scheduler to be fed to
    :py:meth:`debusine.tasks.Task.can_run_on` and determine if a task is
    suitable to be executed on the worker.
    """
    return {
        f"executor:{backend}:available": executor_class.available()
        for backend, executor_class in _backends.items()
    }
