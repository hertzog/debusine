# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the ImageCache class."""

from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp
from unittest import TestCase
from unittest.mock import create_autospec

from debusine.client.debusine import Debusine
from debusine.tasks.executors.images import ImageCache
from debusine.tasks.tests.helper_mixin import TaskHelperMixin


class ImageCacheMetadataTests(TaskHelperMixin, TestCase):
    """Unit tests for ImageCache class that don't work with images."""

    def setUp(self):
        """Mock the Debusine API for tests."""
        self.debusine_api = create_autospec(Debusine)
        self.image_cache = ImageCache(self.debusine_api)
        self.artifact = self.fake_system_tarball_artifact()
        self.debusine_api.artifact_get.return_value = self.artifact

    def test_image_artifact(self):
        """Test that image_metadata fetches the Artifact."""
        self.assertEqual(self.image_cache.image_artifact(42), self.artifact)

    def test_image_artifact_incorrect_category(self):
        """Test that image_artifact rejects the wrong kind of artifact."""
        self.artifact.category = "debian:package-build-log"

        with self.assertRaisesRegex(
            ValueError, r"^Unexpected artifact type: debian:package-build-log$"
        ):
            self.image_cache.image_artifact(42)


class ImageCacheTests(ImageCacheMetadataTests):
    """Unit tests for ImageCache class that work with images."""

    def setUp(self):
        """Mock the cache directory for tests."""
        super().setUp()
        self.cache_path = Path(mkdtemp(prefix="debusine-testsuite-images-"))
        self.addCleanup(rmtree, self.cache_path)
        self.image_cache.image_cache_path = self.cache_path

    def test_download_image_uncached(self):
        """Test that download_image downloads images when not cached."""
        image = self.image_cache.download_image(self.artifact)
        self.assertEqual(image, self.cache_path / "42" / "system.tar.xz")
        self.debusine_api.download_artifact.assert_called_with(
            42, destination=self.cache_path / "42"
        )

    def test_download_image_cached(self):
        """Test that download_image uses cached images."""
        image_path = self.cache_path / "42" / "system.tar.xz"
        image_path.parent.mkdir(parents=True)
        image_path.write_bytes(b"Fake Data")
        self.assertEqual(
            self.image_cache.download_image(self.artifact), image_path
        )
        self.debusine_api.download_artifact.assert_not_called()
