# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the Executor basic interface."""

from unittest import TestCase

from debusine.tasks.executors.base import (
    _backends,
    analyze_worker_all_executors,
)
from debusine.test import TestHelpersMixin


class ExecutorTests(TestHelpersMixin, TestCase):
    """Unit tests for the Executor basic interface."""

    def test_backends_populated(self):
        """Test that we have populated some backends."""
        self.assertGreater(len(_backends), 0)

    def test_analyze_worker_all_executors_no_backends_available(self):
        """analyze_worker_all_executors copes with no available backends."""
        self.mock_is_command_available({})
        executors_metadata = analyze_worker_all_executors()
        self.assertNotEqual(executors_metadata, {})
        for key, value in executors_metadata.items():
            self.assertRegex(key, r"^executor:.*:available$")
            self.assertFalse(value)

    def test_analyze_worker_all_executors_unshare_available(self):
        """analyze_worker_all_executors reports available backends."""
        self.mock_is_command_available({"unshare": True})
        executors_metadata = analyze_worker_all_executors()
        self.assertTrue(executors_metadata["executor:unshare:available"])
