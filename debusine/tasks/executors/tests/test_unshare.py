# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the UnshareExecutor class."""

from pathlib import Path
from unittest import TestCase
from unittest.mock import MagicMock

from debusine.client.debusine import Debusine
from debusine.tasks.executors.base import executor
from debusine.tasks.executors.unshare import UnshareExecutor
from debusine.tasks.tests.helper_mixin import TaskHelperMixin
from debusine.test import TestHelpersMixin


class UnshareExecutorTests(TestHelpersMixin, TaskHelperMixin, TestCase):
    """Unit tests for UnshareExecutor."""

    def setUp(self):
        """Mock the Debusine API for tests."""
        self.debusine_api = MagicMock(spec=Debusine)
        self.image_artifact = self.mock_image_download(self.debusine_api)

    def test_backend_name(self):
        """Test that the backend_name attribute was set."""
        self.assertEqual(UnshareExecutor.backend_name, "unshare")

    def test_available(self):
        """Test that available() returns True if unshare is available."""
        self.mock_is_command_available({"unshare": True})
        self.assertTrue(UnshareExecutor.available())

    def test_available_no_unshare(self):
        """Test that available() returns False if unshare is not available."""
        self.mock_is_command_available({"unshare": False})
        self.assertFalse(UnshareExecutor.available())

    def test_instantiation_fetches_artifact(self):
        """Test that instantiating UnshareExecutor fetches the artifact."""
        image = UnshareExecutor(self.debusine_api, 42)
        self.assertEqual(image.system_image, self.image_artifact)

    def test_download_image(self):
        """Test that download_image calls ImageCache.download_image."""
        image = UnshareExecutor(self.debusine_api, 42)
        image.download_image()
        self.assertEqual(
            image._local_path, self.image_cache_path / "42/system.tar.xz"
        )

    def test_autopkgtest_virt_args(self):
        """Test that autopkgtest_virt_args returns sane arguments."""
        image = UnshareExecutor(self.debusine_api, 42)
        image._local_path = Path("/tmp/system.tar.xz")
        self.assertEqual(
            image.autopkgtest_virt_args(),
            [
                "--arch",
                "amd64",
                "--release",
                "bookworm",
                "--tarball",
                "/tmp/system.tar.xz",
            ],
        )

    def test_autopkgtest_virt_args_before_download(self):
        """Test that autopkgtest_virt_args requires download."""
        image = UnshareExecutor(self.debusine_api, 42)
        with self.assertRaisesRegex(
            Exception, r"^Image must be downloaded before use\.$"
        ):
            image.autopkgtest_virt_args()

    def test_executor_instantiates_unshare(self):
        """Test that executor() supports unshare."""
        instance = executor(self.debusine_api, "unshare", 42)
        self.assertIsInstance(instance, UnshareExecutor)
