# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""The unshare executor backend."""

from pathlib import Path, PurePath
from subprocess import CompletedProcess
from typing import Optional

from debusine import utils
from debusine.client.debusine import Debusine
from debusine.client.models import ArtifactResponse
from debusine.tasks.executors.base import ExecutorInterface, InstanceInterface
from debusine.tasks.executors.images import ImageCache


class UnshareExecutor(ExecutorInterface, backend_name="unshare"):
    """Support the unshare(1) executor."""

    system_image: ArtifactResponse
    _image_cache: ImageCache
    _local_path: Optional[Path] = None

    def __init__(
        self,
        debusine_api: Debusine,
        system_image_id: int,
    ):
        """
        Instantiate an UnshareExecutor.

        :param debusine_api: The object to use the debusine client API.
        :param system_image_id: An artifact id pointing to the system tarball.
        """
        self._image_cache = ImageCache(debusine_api)
        self.system_image = self._image_cache.image_artifact(system_image_id)

    @classmethod
    def available(cls) -> bool:
        """Determine whether this executor is available for operation."""
        return utils.is_command_available("unshare")

    def download_image(self) -> str:
        """
        Make the image available locally.

        Fetch the image from artifact storage, if it isn't already available,
        and make it available locally.

        Return a path to the image or name, as appropriate for the backend.
        """
        self._local_path = self._image_cache.download_image(self.system_image)
        return str(self._local_path)

    def create(self) -> "UnshareInstance":
        """Extract the image to prepare for execution."""
        raise NotImplementedError("Not yet implemented")

    def autopkgtest_virt_args(self) -> list[str]:
        """Generate the arguments to pass to autopkgtest-virt-$backend."""
        if self._local_path is None:
            raise Exception("Image must be downloaded before use.")
        return [
            "--arch",
            self.system_image.data["architecture"],
            "--release",
            self.system_image.data["codename"],
            "--tarball",
            str(self._local_path),
        ]


class UnshareInstance(InstanceInterface):
    """Support instances of the unshare(1) executor."""

    def start(self) -> None:
        """Start the instance, making it ready to run commands."""
        raise NotImplementedError("Not yet implemented")

    def stop(self) -> None:
        """Stop the instance."""
        raise NotImplementedError("Not yet implemented")

    def file_push(self, source: Path, target: PurePath) -> None:
        """Copy a file into the instance."""
        raise NotImplementedError("Not yet implemented")

    def file_pull(self, source: PurePath, target: Path) -> None:
        """Copy a file out of the instance."""
        raise NotImplementedError("Not yet implemented")

    def run(self, *args: list[str], **kwargs) -> CompletedProcess:
        """
        Run a command in the instance.

        Arguments behave as if passed to `subprocess.run`.
        """
        raise NotImplementedError("Not yet implemented")
