# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Image handling for executor backends."""

import logging
from pathlib import Path

from fasteners import InterProcessLock  # type: ignore

from debusine.client.debusine import Debusine
from debusine.client.models import ArtifactResponse

log = logging.getLogger(__name__)


class ImageCache:
    """Download image artifacts to a local cache."""

    image_cache_path: Path = Path.home() / "system-images"

    def __init__(self, debusine_api: Debusine, **kwargs):
        """
        Instantiate an ImageCache.

        debusine_api is the object to use the debusine client API
        """
        self.debusine_api = debusine_api

    def _cache_path(self, artifact_id: int) -> Path:
        return self.image_cache_path / str(artifact_id)

    def image_artifact(self, artifact_id: int) -> ArtifactResponse:
        """Retrieve the Artifact for artifact_id."""
        r = self.debusine_api.artifact_get(artifact_id)
        if r.category not in (
            "debian:system-tarball",
            "debian:system-image",
        ):
            raise ValueError(f"Unexpected artifact type: {r.category}")
        return r

    def _image_lock(self, artifact_id: int):
        """Context Manager to hold a RW lock for working on a cached image."""
        cache_path = self._cache_path(artifact_id)
        cache_path.mkdir(exist_ok=True)
        return InterProcessLock(cache_path / ".lock")

    def download_image(self, artifact: ArtifactResponse) -> Path:
        """
        Make the image available locally.

        Fetch the image from artifact storage, if it isn't already available,
        and make it available locally.

        Return a path to the image or name, as appropriate for the backend.
        """
        self.image_cache_path.mkdir(exist_ok=True)

        filename = artifact.data["filename"]
        cache_path = self._cache_path(artifact.id)
        path = cache_path / filename

        with self._image_lock(artifact.id):
            if path.exists():
                return path
            self.debusine_api.download_artifact(
                artifact.id, destination=cache_path
            )
        # TODO: Implement use-tracking and cleanup.
        return path
