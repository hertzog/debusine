# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Ontology definition of SystemBootstrap."""
import abc
import tempfile
from pathlib import Path

from debian.deb822 import Deb822

from debusine.client.exceptions import ContentValidationError
from debusine.client.utils import get_url_contents_sha256sum
from debusine.tasks import Task


class SystemBootstrap(abc.ABC, Task):
    """Implement ontology SystemBootstrap."""

    TASK_VERSION = "1.0"
    TASK_DATA_SCHEMA = {
        "type": "object",
        "required": ["bootstrap_options", "bootstrap_repositories"],
        "additionalProperties": False,
        "properties": {
            "bootstrap_options": {
                "type": "object",
                "additionalProperties": False,
                "required": ["architecture"],
                "properties": {
                    "variant": {
                        "type": "string",
                        "enum": [
                            "buildd",
                            "minbase",
                        ],
                    },
                    "extra_packages": {
                        "type": "array",
                        "items": {"type": "string"},
                    },
                    "architecture": {"type": "string"},
                },
            },
            "bootstrap_repositories": {
                "type": "array",
                "items": {
                    "type": "object",
                    "additionalProperties": False,
                    "required": [
                        "mirror",
                        "suite",
                    ],
                    "properties": {
                        "types": {
                            "type": "array",
                            "minItems": 1,
                            "uniqueItems": True,
                            "items": {
                                "type": "string",
                                "enum": ["deb", "deb-src"],
                            },
                        },
                        "mirror": {"type": "string"},
                        "suite": {
                            "type": "string",
                        },
                        "components": {
                            "type": "array",
                            "uniqueItems": True,
                            "items": {
                                "type": "string",
                            },
                        },
                        "check_signature_with": {
                            "type": "string",
                            "enum": ["system", "external", "no-check"],
                        },
                        "keyring_package": {"type": "string"},
                        "keyring": {
                            "type": "object",
                            "additionalProperties": False,
                            "required": ["url"],
                            "properties": {
                                "url": {"type": "string", "format": "uri"},
                                "sha256sum": {"type": "string"},
                                "install": {"type": "boolean"},
                            },
                        },
                    },
                },
            },
            "customization_script": {
                "type": "string",
            },
        },
    }

    def __init__(self):
        """Initialize object."""
        super().__init__()

    def configure(self, task_data):
        """Handle SystemBootstrap configuration."""
        super().configure(task_data)

        # Handle default values
        for i, repository in enumerate(self.data["bootstrap_repositories"]):
            repository.setdefault("types", ["deb"])
            repository.setdefault("check_signature_with", "system")
            if (keyring := repository.get("keyring")) is not None:
                keyring.setdefault("install", False)

            if repository["check_signature_with"] == "external":
                if "keyring" not in repository:
                    raise ValueError(
                        f"repository['{i}'] requires 'keyring': "
                        f"'check_signature_with' is set to 'external'"
                    )

    @staticmethod
    def _deb822_source(
        repository: dict, *, keyring_directory: Path, use_signed_by: bool
    ) -> Deb822:
        """
        Create a deb822 from repository and return it.

        :raise: ContentValidationError if the repository["keyring"]["sha256sum"]
          does not match the one from the downloaded keyring
        :param keyring_directory: directory to save the gpg keys
        :param use_signed_by: add Signed-By in the repository
        :return: repository
        """
        deb822 = Deb822()

        deb822["Types"] = " ".join(repository["types"])
        deb822["URIs"] = repository["mirror"]
        deb822["Suites"] = repository["suite"]
        deb822["Components"] = " ".join(repository["components"])

        if repository["check_signature_with"] == "no-check":
            deb822["Trusted"] = "yes"

        if repository["check_signature_with"] == "external":
            keyring, actual_sha256sum = get_url_contents_sha256sum(
                repository["keyring"]["url"], 100 * 1024 * 1024
            )

            if (expected := repository["keyring"].get("sha256sum")) is not None:
                if actual_sha256sum != expected:
                    raise ContentValidationError(
                        f"sha256 mismatch for keyring repository "
                        f"{repository['mirror']}. "
                        f"Actual: {actual_sha256sum} expected: {expected}"
                    )

            # Disable auto-deletion: the file will be deleted together
            # with keyring_directory when the Task finishes
            file = tempfile.NamedTemporaryFile(
                dir=keyring_directory,
                prefix="keyring-repo-",
                suffix=".asc",
                delete=False,
            )
            Path(file.name).write_bytes(keyring)

            if use_signed_by:
                deb822["Signed-By"] = file.name

        return deb822

    @classmethod
    def _write_deb822s(cls, deb822s: list[Deb822], destination: Path):
        with destination.open("wb") as f:
            for deb822 in deb822s:
                deb822.dump(f)
                f.write(b"\n")

    def _generate_deb822_sources(
        self,
        repositories: list[dict],
        *,
        keyrings_dir: Path,
        use_signed_by: bool,
    ) -> list[Deb822]:
        """
        Return list of Deb822 repositories.

        :param keyrings_dir: write gpg keys into it.
        :use_signed_by: if True, add Signed-By to the repository with the path
          to the file.
        """
        deb822s = []
        for repository in repositories:
            deb822s.append(
                self._deb822_source(
                    repository,
                    keyring_directory=keyrings_dir,
                    use_signed_by=use_signed_by,
                )
            )

        return deb822s

    @abc.abstractmethod
    def _cmdline(self) -> list[str]:
        raise NotImplementedError()
