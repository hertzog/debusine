# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Task to test Debian binary packages using piuparts.

This task follows the specification laid out here:
https://freexian-team.pages.debian.net/debusine/reference/tasks.html#piuparts-task
"""

from pathlib import Path

import debusine.utils
from debusine.tasks import Task
from debusine.tasks._task_mixins import (
    FetchExecUploadMixin,
    TaskRunCommandMixin,
)


class Piuparts(TaskRunCommandMixin, FetchExecUploadMixin, Task):
    """Test Debian binary packages using piuparts."""

    TASK_VERSION = 1
    TASK_DATA_SCHEMA = {
        "type": "object",
        "properties": {
            "input": {
                "type": "object",
                "properties": {
                    "binary_artifacts_ids": {
                        "type": "array",
                        "items": {"type": "integer"},
                    },
                },
                "required": ["binary_artifacts_ids"],
                "additionalProperties": False,
            },
            # schroot selection
            "distribution": {
                "type": "string",
            },
            "host_architecture": {
                "type": "string",
            },
        },
        "required": ["input", "distribution", "host_architecture"],
        "additionalProperties": False,
    }

    def __init__(self) -> None:
        """Initialize (constructor)."""
        super().__init__()
        # *.deb paths. Set by self.configure_for_execution()
        self._deb_files: list[Path] = []

    def analyze_worker(self):
        """Report metadata for this task on this worker."""
        metadata = super().analyze_worker()

        available_key = self.prefix_with_task_name("available")
        # Don't assume /usr/sbin is on $PATH.
        metadata[available_key] = debusine.utils.is_command_available(
            "/usr/sbin/piuparts"
        )

        return metadata

    def can_run_on(self, worker_metadata: dict) -> bool:
        """Check if the specified worker can run the task."""
        available_key = self.prefix_with_task_name("available")
        return worker_metadata.get(available_key, False)

    def fetch_input(self, destination: Path) -> bool:
        # FetchBuildUploadMixin method
        """Populate work directory with user-specified binary artifact(s)."""
        for artifact_id in self.data["input"]["binary_artifacts_ids"]:
            ret = self.fetch_artifact(artifact_id, destination)
            if not ret:
                return False
        return True

    def configure_for_execution(self, download_directory: Path) -> bool:
        # FetchBuildUploadMixin method
        """Set and validate variables for self._cmdline()."""
        self._deb_files = debusine.utils.find_files_suffixes(
            download_directory, [".deb"]
        )
        # Ensure we've got >=1 .deb file(s)
        # Note: make Task.find_file_by_suffix() more generic and factor code
        if len(self._deb_files) == 0:
            list_of_files = sorted(map(str, download_directory.iterdir()))
            self.append_to_log_file(
                "configure_for_execution.log",
                [
                    f"There must be at least one *.deb file. "
                    f"Current files: {list_of_files}"
                ],
            )
            return False
        return True

    def _cmdline(self):
        # FetchBuildUploadMixin method
        """Build full piuparts command line."""
        cmdline = []

        # Isolation: use sudo, pending #189 conclusion
        cmdline.append("sudo")
        cmdline.append("/usr/sbin/piuparts")
        # Always set distribution, otherwise it defaults to 'sid'
        cmdline.append(f"--distribution={self.data['distribution']}")
        # Use schroots from the sbuild task, pending #189 conclusion;
        # also supports non-standard environments/keyrings such as ELTS
        cmdline.append(
            f"--schroot={self.data['distribution']}"
            f"-{self.data['host_architecture']}"
            "-sbuild"
        )
        # Common options used in Debian piuparts services/CI
        cmdline.append("--allow-database")
        cmdline.append("--warn-on-leftovers-after-purge")
        # Maintenance scripts requirement
        cmdline.append("--tmpdir=/var/cache/piuparts/tmp")

        cmdline.extend(map(str, self._deb_files))
        return cmdline

    def upload_artifacts(
        self, build_directory: Path, *, execution_success: bool  # noqa: U100
    ):
        # FetchBuildUploadMixin method
        """cmd-output.log is enough for now, upload nothing."""
        return True
