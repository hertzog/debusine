# Copyright 2022, 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Mixins for the Task class."""

import os
import signal
import subprocess
import tempfile
import traceback
from contextlib import contextmanager
from pathlib import Path
from typing import BinaryIO, Iterator, Optional

from debusine.client.models import ArtifactResponse


class FetchExecUploadMixin:
    r"""
    Fetch data for the tasks, execute self._cmdline() and upload artifacts.

    In order to use this mixin, tasks must implement:
    - ``configure_for_execution(self, download_directory: Path) -> bool``
    - ``_cmdline(self) -> list[str]``
    - ``run_cmd(cmd, execute_directory, capture_stdout_filename) -> bool``
      (usually through ``TaskRunCommandMixin``)
    - ``upload_artifacts(self, directory: Path, \*, execution_success: bool)``.
      The member variable self._source_artifacts_ids is set by
      ``fetch_input()`` and can be used to create the relations between
      uploaded artifacts and downloaded artifacts.
    - ``fetch_input(self, destination) -> bool`` . Download the needed artifacts
      into destination. Suggestion: can use fetch_artifact(artifact_id, dir)
      to download them.

    - ``check_directory_for_consistency_errors(self, build_directory: Path)
      -> list[str]`` (defaults return an empty list: no-errors)
    - ``task_succeeded(self, execute_directory: Path) -> bool`` (defaults to
        True)

    Use `self.append_to_log_file()` / `self.open_debug_log_file()` to provide
    information for the user (it will be available as artifact to the user).

    See the main entry point `_execute()` for the details in the
    FetchExecUploadMixin flow.
    """  # noqa: RST301, RST201

    # If CAPTURE_OUTPUT_FILENAME is not None: self._execute() create a file
    # in the cwd of the command to save the stdout. The file is available
    # in the self.upload_artifacts().
    CAPTURE_OUTPUT_FILENAME: Optional[str] = None

    def _execute(self) -> bool:
        """
        Fetch the required input, execute the command and upload artifacts.

        Flow is:

        - Call ``self.fetch_input():`` download the artifacts
        - Call ``self.configure_for_execution()``: to set any member variables
          that might be used in ``self._cmdline()``
        - Run the command, capturing the stdout (see CAPTURE_OUTPUT_FILENAME)
          in the newly created execute_directory
        - Call ``run_cmd_succeeded()``: if return False: early exit, if return
          True continue the execution of the method
        - Call ``check_directory_for_consistency_errors()``. If any errors are
          returned save them into consistency.log.
        - Call ``self.task_succeeded()``.
        - Call ``self.upload_artifacts(exec_dir, execution_success=succeeded)``.
        - Return ``execution_succeeded`` (the Worker will set the result as
          "Success" or "Failure")

        .. note::

          If the member variable CAPTURE_OUTPUT_FILENAME is set:
          create a file with its name with the stdout of the command. Otherwise,
          the stdout of the command is saved in self.CMD_LOG_FILENAME).
        """
        with (
            self._temporary_directory() as execute_directory,
            self._temporary_directory() as download_directory,
        ):
            if not self.fetch_input(download_directory):
                return False

            try:
                if not self.configure_for_execution(download_directory):
                    return False
            except Exception as exc:
                exc_type = type(exc).__name__
                exc_traceback = traceback.format_exc()

                log_message = [
                    f"Exception type: {exc_type}",
                    f"Message: {exc}",
                    "",
                    exc_traceback,
                ]

                self.append_to_log_file(
                    "configure_for_execution.log", log_message
                )
                return False

            cmd = self._cmdline()
            self.logger.info("Executing: %s", " ".join(cmd))

            returncode = self.run_cmd(
                cmd,
                execute_directory,
                capture_stdout_filename=self.CAPTURE_OUTPUT_FILENAME,
            )

            if not self.run_cmd_succeeded(returncode):
                return False

            if errors := self.check_directory_for_consistency_errors(
                execute_directory
            ):
                self.append_to_log_file("consistency.log", sorted(errors))
                return False

            execution_succeeded = self.task_succeeded(execute_directory)

            self.upload_artifacts(
                execute_directory, execution_success=execution_succeeded
            )

            return execution_succeeded

    def task_succeeded(self, execute_directory: Path) -> bool:  # noqa: U100
        """
        Sub-tasks can evaluate if the task was a success or failure.

        By default, return True (success). Sub-classes can re-implement it.

        :param execute_directory: directory with the output of the task
        :return: True (if success) or False (if failure).
        """
        return True

    def run_cmd_succeeded(self, returncode: Optional[int]) -> bool:
        """Return True if returncode == 0."""
        return returncode == 0

    def configure_for_execution(self, download_directory: Path) -> bool:
        """
        Configure task: set variables needed for the self._cmdline().

        Called after the files are downloaded via `fetch_input()`.
        """
        raise NotImplementedError()

    def run_cmd(
        self,
        cmd: list[str],
        working_directory: Path,
        *,
        capture_stdout_filename: Optional[str] = None,
    ) -> Optional[int]:
        """
        Run cmd in working_directory.

        :param cmd: command to execute with its arguments.
        :param working_directory: working directory where the command
          is executed.
        :param capture_stdout_filename: for some commands the output of the
          command is the output of stdout (e.g. lintian) and not a set of files
          generated by the command (e.g. sbuild). If capture_stdout is not None,
          save the stdout into this file. The caller can then use it.
        :return: returncode of the process or None if aborted
        """
        raise NotImplementedError()

    def check_directory_for_consistency_errors(
        self, build_directory: Path  # noqa: U100
    ) -> list[str]:
        """Return list of errors after executing the command."""
        return []

    def _cmdline(self) -> list[str]:
        raise NotImplementedError()

    def fetch_input(self, destination: Path) -> bool:
        """
        Download artifacts needed by the Task, update self.source_artifacts_ids.

        Task might use self.data["input"] to download the relevant artifacts.

        The method self.download_artifact(artifact_id, destination) might be
        used to download the relevant artifacts and update
        self.source_artifacts_ids.
        """
        raise NotImplementedError()

    def upload_artifacts(
        self, execute_directory: Path, *, execution_success: bool
    ):
        """Upload the artifacts for the task."""
        raise NotImplementedError()

    def fetch_artifact(
        self, artifact_id: int, destination: Path
    ) -> ArtifactResponse:
        """
        Download artifact_id to destination.

        Add artifact_id to self._source_artifacts_ids.
        """
        artifact_response = self.debusine.download_artifact(
            artifact_id, destination, tarball=False
        )
        self._source_artifacts_ids.append(artifact_id)
        return artifact_response

    @staticmethod
    @contextmanager
    def _temporary_directory() -> Iterator[Path]:
        with tempfile.TemporaryDirectory(
            prefix="debusine-fetch-exec-upload-"
        ) as directory:
            yield Path(directory)


class TaskRunCommandMixin:
    """
    Mixin to run commands.

    Use process groups to make sure that the command and possible spawned
    commands are finished and if Task.aborted() is True cancels the
    execution of the command.
    """

    CMD_LOG_SEPARATOR = "--------------------"
    CMD_LOG_FILENAME = "cmd-output.log"

    @staticmethod
    def _write_utf8(file: BinaryIO, text: str) -> None:
        file.write(text.encode("utf-8", errors="replace") + b"\n")

    def _write_popen_result(self, file: BinaryIO, p: subprocess.Popen):
        self._write_utf8(file, f"\naborted: {self.aborted}")
        self._write_utf8(file, f"returncode: {p.returncode}")

    def run_cmd(
        self,
        cmd: list[str],
        working_directory: Path,
        *,
        capture_stdout_filename: Optional[str] = None,
    ) -> Optional[int]:
        """
        Run cmd in working_directory. Create self.CMD_OUTPUT_FILE log file.

        If Task.cancelled == True terminates the process.

        :param cmd: command to execute with its arguments.
        :param working_directory: working directory where the command
          is executed.
        :param capture_stdout_filename: for some commands the output of the
          command is the output of stdout (e.g. lintian) and not a set of files
          generated by the command (e.g. sbuild). If capture_stdout is not None,
          save the stdout into this file. The caller can then use it.
        :return: returncode of the process or None if aborted
        """
        with self.open_debug_log_file(
            self.CMD_LOG_FILENAME, mode="ab"
        ) as cmd_log:
            cmd_quoted = self._quote_cmd(cmd)
            self._write_utf8(cmd_log, f"cmd: {cmd_quoted}")

            if capture_stdout_filename:
                self._write_utf8(
                    cmd_log,
                    "output (contains stderr only, stdout was captured):",
                )
                capture_stdout = working_directory / capture_stdout_filename
                out_file = capture_stdout.open(mode="wb")
            else:
                self._write_utf8(
                    cmd_log, "output (contains stdout and stderr):"
                )
                out_file = cmd_log

            try:
                return self._run_cmd(
                    cmd,
                    working_directory,
                    cmd_log=cmd_log,
                    out_file=out_file,
                )
            finally:
                file_names = "\n".join(
                    str(file.relative_to(working_directory))
                    for file in sorted(working_directory.rglob("*"))
                )

                self._write_utf8(cmd_log, "\nFiles in working directory:")
                self._write_utf8(cmd_log, file_names)

                self._write_utf8(cmd_log, self.CMD_LOG_SEPARATOR)

                out_file.close()

    def _run_cmd(
        self,
        cmd: list[str],
        working_directory: Path,
        *,
        cmd_log: BinaryIO,
        out_file: BinaryIO,
    ) -> Optional[int]:
        """
        Execute cmd.

        :param cmd_log: save the command log (parameters, stderr, return code,
          stdout)
        :param out_file: save the command stdout (might be the same as
          cmd_log or a different file)

        :return: returncode of the process or None if aborted
        """
        # Need to flush ro subprocess.Popen() overwrites part of it
        cmd_log.flush()
        out_file.flush()

        p = subprocess.Popen(
            cmd,
            start_new_session=True,
            cwd=working_directory,
            stderr=cmd_log.fileno(),
            stdout=out_file.fileno(),
        )
        process_group = os.getpgid(p.pid)

        while True:
            if self.aborted:
                break

            try:
                self._wait_popen(p, timeout=1)
                break
            except subprocess.TimeoutExpired:
                pass

        if self.aborted:
            self.logger.debug("Task (cmd: %s PID %s) aborted", cmd, p.pid)
            try:
                if not self._send_signal_pid(p.pid, signal.SIGTERM):
                    # _send_signal_pid failed probably because cmd finished
                    # after aborting and before sending the signal
                    #
                    # p.poll() to read the returncode and avoid leaving cmd
                    # as zombie
                    p.poll()

                    # Kill possible processes launched by cmd
                    self._send_signal_group(process_group, signal.SIGKILL)
                    self.logger.debug("Could not send SIGTERM to %s", p.pid)

                    self._write_popen_result(cmd_log, p)
                    return None

                # _wait_popen with a timeout=5 to leave 5 seconds of grace
                # for the cmd to finish after sending SIGTERM
                self._wait_popen(p, timeout=5)
            except subprocess.TimeoutExpired:
                # SIGTERM was sent and 5 seconds later cmd
                # was still running. A SIGKILL to the process group will
                # be sent
                self.logger.debug(
                    "Task PID %s not finished after SIGTERM", p.pid
                )
                pass

            # debusine sends a SIGKILL if:
            # - SIGTERM was sent to cmd AND cmd was running 5 seconds later:
            #   SIGTERM was not enough so SIGKILL to the group is needed
            # - SIGTERM was sent to cmd AND cmd finished: SIGKILL to the
            #   group to make sure that there are not processes spawned
            #   by cmd running
            # (note that a cmd could launch processes in a new group
            # could be left running)
            self._send_signal_group(process_group, signal.SIGKILL)
            self.logger.debug("Sent SIGKILL to process group %s", process_group)

            # p.poll() to set p.returncode and avoid leaving cmd
            # as a zombie process.
            # But cmd might be left as a zombie process: if cmd was in a
            # non-interruptable kernel call p.returncode will be None even
            # after p.poll() and it will be left as a zombie process
            # (until debusine worker dies and the zombie is adopted by
            # init and waited on by init). If this happened there we might be a
            # ResourceWarning from Popen.__del__:
            # "subprocess %s is still running"
            #
            # A solution would e to wait (p.waitpid()) that the
            # process finished dying. This is implemented in the unit test
            # to avoid the warning but not implemented here to not delay
            # the possible shut down of debusine worker
            p.poll()
            self.logger.debug("Returncode for PID %s: %s", p.pid, p.returncode)

            return None
        else:
            # The cmd has finished. The cmd might have spawned
            # other processes. debusine will kill any alive processes.
            #
            # If they existed they should have been finished by cmd:
            # run_cmd() should not leave processes behind.
            #
            # Since the parent died they are adopted by init and on
            # killing them they are not zombie.
            # (cmd might have spawned new processes in a different process
            # group: if this is the case they will be left running)
            self._send_signal_group(process_group, signal.SIGKILL)

        self._write_popen_result(cmd_log, p)

        return p.returncode

    def _wait_popen(self, popen: subprocess.Popen, timeout: float):
        return popen.wait(timeout)

    @staticmethod
    def _send_signal_pid(pid, signal) -> bool:
        try:
            os.kill(pid, signal)
        except ProcessLookupError:
            return False

        return True

    @staticmethod
    def _send_signal_group(process_group, signal):
        """Send signal to the process group."""
        try:
            os.killpg(process_group, signal)
        except ProcessLookupError:
            pass
