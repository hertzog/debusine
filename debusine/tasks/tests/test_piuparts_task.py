# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the piuparts task support on the worker."""
from pathlib import Path
from unittest import TestCase, mock

from debusine.tasks import TaskConfigError
from debusine.tasks.piuparts import Piuparts
from debusine.tasks.tests.helper_mixin import TaskHelperMixin
from debusine.test import TestHelpersMixin


class PiupartsTaskTests(TestHelpersMixin, TaskHelperMixin, TestCase):
    """Test the Piuparts Task class."""

    SAMPLE_TASK_DATA = {
        "input": {"binary_artifacts_ids": [421]},
        "distribution": "bookworm",
        "host_architecture": "amd64",
    }

    def setUp(self):  # noqa: D102
        self.task = Piuparts()
        self.task.configure(self.SAMPLE_TASK_DATA)

    def tearDown(self):
        """Delete directory to avoid ResourceWarning with python -m unittest."""
        if self.task._debug_log_files_directory is not None:
            self.task._debug_log_files_directory.cleanup()

    def test_configure_fails_with_missing_required_data(self):  # noqa: D102
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"input": {"artifact_id": 1}})

    def test_analyze_worker(self):
        """Test the analyze_worker() method."""
        self.mock_is_command_available({"/usr/sbin/piuparts": True})
        metadata = self.task.analyze_worker()
        self.assertEqual(metadata["piuparts:available"], True)

    def test_analyze_worker_piuparts_not_available(self):
        """analyze_worker() handles piuparts not being available."""
        self.mock_is_command_available({"/usr/sbin/piuparts": False})
        metadata = self.task.analyze_worker()
        self.assertEqual(metadata["piuparts:available"], False)

    def test_can_run_on(self):
        """can_run_on returns True if piuparts is available."""
        self.assertTrue(self.task.can_run_on({"piuparts:available": True}))

    def test_can_run_on_missing_tool(self):
        """can_run_on returns False if piuparts is not available."""
        self.assertFalse(self.task.can_run_on({"piuparts:available": False}))

    def test_configure_for_execution_from_artifact_id_error_no_debs(self):
        """configure_for_execution() no .deb-s: return False."""
        download_directory = self.create_temporary_directory()
        (file1 := download_directory / "file1.dsc").write_text("")
        (file2 := download_directory / "file2.changes").write_text("")

        self.assertFalse(self.task.configure_for_execution(download_directory))

        files = sorted([str(file1), str(file2)])
        log_file_contents = (
            Path(self.task._debug_log_files_directory.name)
            / "configure_for_execution.log"
        ).read_text()
        self.assertEqual(
            log_file_contents,
            f"There must be at least one *.deb file. "
            f"Current files: {files}\n",
        )

    def test_execute(self):
        """Test full (mocked) execution."""
        # Full execution
        self.configure_task(
            override={"input": {'binary_artifacts_ids': [1, 2]}}
        )
        download_directory = self.create_temporary_directory()

        with mock.patch.object(
            self.task, "fetch_artifact", autospec=True, return_value=False
        ):
            self.assertFalse(self.task.fetch_input(download_directory))

        with mock.patch.object(
            self.task, "fetch_artifact", autospec=True, return_value=True
        ) as mocked:
            self.assertTrue(self.task.fetch_input(download_directory))
            mocked.assert_any_call(1, download_directory)
            mocked.assert_any_call(2, download_directory)

        (file2 := download_directory / "file2.deb").write_text("")
        (file1 := download_directory / "file1.deb").write_text("")
        (file3 := download_directory / "makedev_2.3.1-97_all.deb").write_text(
            ""
        )
        self.assertTrue(self.task.configure_for_execution(download_directory))
        self.assertEqual(self.task._deb_files, [file1, file2, file3])
        self.assertEqual(
            self.task._cmdline(),
            [
                "sudo",
                "/usr/sbin/piuparts",
                "--distribution=bookworm",
                "--schroot=bookworm-amd64-sbuild",
                "--allow-database",
                "--warn-on-leftovers-after-purge",
                "--tmpdir=/var/cache/piuparts/tmp",
                str(file1),
                str(file2),
                str(file3),
            ],
        )

        self.assertTrue(
            self.task.upload_artifacts(
                download_directory, execution_success=True
            )
        )
