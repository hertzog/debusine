# Copyright 2021 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Task module unit tests for debusine."""

from debusine.tasks.tests.test_task_mixins import (  # noqa: F401, I202
    TaskWithRunCommandMixin,
)
