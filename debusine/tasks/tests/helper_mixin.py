# Copyright 2022-2023 The Debusine developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Common test-helper code involving Tasks."""
import datetime
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp
from typing import Any, Optional
from unittest import mock
from unittest.mock import MagicMock

try:
    import pydantic.v1 as pydantic
except ImportError:
    import pydantic as pydantic  # type: ignore

from debusine.client.debusine import Debusine
from debusine.client.models import (
    ArtifactResponse,
    FileResponse,
    FilesResponseType,
    NonNegativeInt,
    StrMaxLength255,
)


class TaskHelperMixin:
    """Helper mixin for Task tests."""

    def configure_task(
        self,
        task_data: Optional[dict[str, Any]] = None,
        override: Optional[dict[str, Any]] = None,
        remove: Optional[list[str]] = None,
    ):
        """
        Run self.task.configure(task_data) with different inputs.

        Copy self.SAMPLE_TASK_DATA or task_data and modify it, call
        self.task.configure(modified_SAMPLE_TASK_DATA).

        :param task_data: if provided use this as a base dictionary. If None,
          use self.SAMPLE_TASK_DATA.
        :param override: dictionary with the keys to modify (only root keys,
          not recursive search) by its values. Modify or add them.
        :param remove: list of keys to remove.
        """
        if task_data is None:  # pragma: no cover
            task_data = self.SAMPLE_TASK_DATA.copy()
        if override:
            for key, value in override.items():
                task_data[key] = value
        if remove:
            for key in remove:
                task_data.pop(key, None)
        self.task.configure(task_data)

    def mock_debusine(self) -> MagicMock:
        """Create a Debusine mock and configure self.task for it. Return it."""
        debusine_mock = mock.create_autospec(spec=Debusine)

        # Worker would set the server
        self.task.configure_server_access(debusine_mock)

        return debusine_mock

    def mock_image_download(self, debusine_mock: MagicMock) -> ArtifactResponse:
        """Configure a fake ImageCache path and fake_system_tarball_artifact."""
        artifact_response = self.fake_system_tarball_artifact()
        debusine_mock.artifact_get.return_value = artifact_response

        self.image_cache_path = Path(
            mkdtemp(prefix="debusine-testsuite-images-")
        )
        self.addCleanup(rmtree, self.image_cache_path)

        self.image_cache_patcher = mock.patch(
            "debusine.tasks.executors.images.ImageCache.image_cache_path",
            self.image_cache_path,
        )
        self.image_cache_patcher.start()
        self.addCleanup(self.image_cache_patcher.stop)

        return artifact_response

    def fake_system_tarball_artifact(self) -> ArtifactResponse:
        """Create a fake ArtifactResponse for a debian:system-tarball."""
        return ArtifactResponse(
            id=42,
            workspace="Testing",
            category="debian:system-tarball",
            created_at=datetime.datetime.now(),
            data={
                "architecture": "amd64",
                "codename": "bookworm",
                "filename": "system.tar.xz",
                "vendor": "debian",
            },
            download_tar_gz_url=pydantic.parse_obj_as(
                pydantic.AnyUrl, "https://example.com/download-42/"
            ),
            files=FilesResponseType(
                {
                    "system.tar.xz": FileResponse(
                        size=pydantic.parse_obj_as(NonNegativeInt, 4001),
                        checksums={
                            "sha256": pydantic.parse_obj_as(
                                StrMaxLength255, "abc123"
                            ),
                        },
                        type="file",
                        url=pydantic.parse_obj_as(
                            pydantic.AnyUrl,
                            "https://example.com/download-system.tar.xz",
                        ),
                    ),
                }
            ),
            files_to_upload=[],
        )
