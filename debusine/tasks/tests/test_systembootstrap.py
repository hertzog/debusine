# Copyright 2023 The Debusine developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for SystemBootstrap class."""
import copy
import hashlib
from pathlib import Path
from typing import Any, Optional
from unittest import TestCase

import jsonschema

import responses

from debusine.client.exceptions import ContentValidationError
from debusine.tasks.systembootstrap import SystemBootstrap
from debusine.tasks.tests.helper_mixin import TaskHelperMixin
from debusine.test import TestHelpersMixin


class SystemBootstrapImpl(SystemBootstrap):
    """Implementation of SystemBootstrap ontology."""

    def _cmdline(self) -> list[str]:
        return []  # pragma: no cover


class SystemBootstrapTests(TestHelpersMixin, TaskHelperMixin, TestCase):
    """Tests SystemBootstrap class."""

    SAMPLE_TASK_DATA = {
        "bootstrap_options": {
            "architecture": "amd64",
            "variant": "buildd",
            "extra_packages": ["hello"],
        },
        "bootstrap_repositories": [
            {
                "mirror": "https://deb.debian.org/deb",
                "suite": "bookworm",
                "components": ["main", "contrib"],
                "check_signature_with": "system",
            },
            {
                "types": ["deb-src"],
                "mirror": "https://example.com",
                "suite": "bullseye",
                "components": ["main"],
                "check_signature_with": "no-check",
            },
        ],
    }

    def setUp(self):
        """Initialize test."""
        self.task = SystemBootstrapImpl()
        self.configure_task()

    def test_configure_task(self):
        """Ensure self.SAMPLE_TASK_DATA is valid."""
        self.configure_task(self.SAMPLE_TASK_DATA)

        # Test default options
        sample_repositories = self.SAMPLE_TASK_DATA["bootstrap_repositories"]
        actual_repositories = self.task.data["bootstrap_repositories"]

        # First sample repository does not have "types" nor
        # "signed_signature_with"
        self.assertNotIn("types", sample_repositories[0])
        self.assertNotIn("signed_signature_with", sample_repositories[0])
        # They are assigned to the default values
        self.assertEqual(actual_repositories[0]["types"], ["deb"])
        self.assertEqual(
            actual_repositories[0]["check_signature_with"], "system"
        )

        #  Second sample repository has "types" / "signed_signature_with" as
        # per SAMPLE_TASK_DATA
        self.assertEqual(sample_repositories[1]["types"], ["deb-src"])
        self.assertEqual(
            actual_repositories[1]["check_signature_with"], "no-check"
        )

    def test_configure_task_raise_value_error_missing_keyring(self):
        """configure_task() raise ValueError: missing keyring parameter."""
        task_data = copy.deepcopy(self.SAMPLE_TASK_DATA)
        task_data["bootstrap_repositories"][0][
            "check_signature_with"
        ] = "external"

        msg = (
            r"repository\['0'\] requires 'keyring': 'check_signature_with' "
            r"is set to 'external'"
        )

        with self.assertRaisesRegex(ValueError, msg):
            self.configure_task(task_data)

    def create_and_validate_repo(
        self,
        check_signature_with: str,
        *,
        keyring_url: Optional[str] = None,
        keyring_sha256sum: Optional[str] = None,
    ):
        """Create a basic repository, validates and return it."""
        repository: dict[str, Any] = {
            "types": ["deb"],
            "mirror": "https://example.com",
            "suite": "bullseye",
            "components": ["main", "contrib"],
            "check_signature_with": check_signature_with,
        }

        if keyring_url is not None:
            repository["keyring"] = {"url": keyring_url}

        if keyring_sha256sum is not None:
            repository["keyring"]["sha256sum"] = keyring_sha256sum

        jsonschema.validate(
            repository,
            self.task.TASK_DATA_SCHEMA["properties"]["bootstrap_repositories"][
                "items"
            ],
        )

        return repository

    def test_deb822_source(self):
        """Generate a correct Deb822 sources file."""
        repository = self.create_and_validate_repo("system")

        actual = self.task._deb822_source(
            repository,
            keyring_directory=self.create_temporary_directory(),
            use_signed_by=True,
        )

        expected = {
            "Types": "deb",
            "URIs": "https://example.com",
            "Suites": "bullseye",
            "Components": "main contrib",
        }

        self.assertEqual(actual, expected)

    def test_deb822_source_signature_no_check(self):
        """Generate Deb822 with "trusted"."""
        repository = self.create_and_validate_repo("no-check")

        actual = self.task._deb822_source(
            repository,
            keyring_directory=self.create_temporary_directory(),
            use_signed_by=True,
        )

        self.assertEqual(actual["Trusted"], "yes")

    @responses.activate
    def test_deb822_source_file_download_signature(self):
        """Generate Deb822 and downloads a signature file."""
        keyring_contents = b"some-keyring"
        keyring_url = "https://example.com/keyring_file.txt"
        keyring_directory = self.create_temporary_directory()

        repository = self.create_and_validate_repo(
            "external", keyring_url=keyring_url
        )

        responses.add(
            responses.GET, repository["keyring"]["url"], body=keyring_contents
        )

        actual = self.task._deb822_source(
            repository, keyring_directory=keyring_directory, use_signed_by=True
        )

        signed_by_keyring = Path(actual["Signed-By"])
        self.assertEqual(signed_by_keyring.read_bytes(), keyring_contents)

        # It was created in the correct directory
        self.assertIs(signed_by_keyring.is_relative_to(keyring_directory), True)

    @responses.activate
    def test_deb822_source_keyring_hash_mismatch(self):
        """Method raise ValueError: keyring sha256 mismatch."""
        keyring_contents = b"some-keyring"
        keyring_url = "https://example.com/keyring_file.txt"
        keyring_directory = self.create_temporary_directory()

        expected_sha256 = "will be a mismatch"

        repository = self.create_and_validate_repo(
            "external",
            keyring_url=keyring_url,
            keyring_sha256sum=expected_sha256,
        )

        responses.add(
            responses.GET, repository["keyring"]["url"], body=keyring_contents
        )

        actual_sha256 = hashlib.sha256(keyring_contents).hexdigest()

        expected = (
            f"sha256 mismatch for keyring repository {repository['mirror']}. "
            f"Actual: {actual_sha256} expected: {expected_sha256}"
        )

        with self.assertRaisesRegex(ContentValidationError, expected):
            self.task._deb822_source(
                repository,
                keyring_directory=keyring_directory,
                use_signed_by=True,
            )

    @responses.activate
    def test_deb822_source_keyring_hash_correct(self):
        """Keyring sha256 is correct."""
        """Method raise ValueError: keyring sha256 mismatch."""
        keyring_contents = b"some-keyring"
        keyring_url = "https://example.com/keyring_file.txt"
        keyring_directory = self.create_temporary_directory()

        actual_sha256 = hashlib.sha256(keyring_contents).hexdigest()

        repository = self.create_and_validate_repo(
            "external",
            keyring_url=keyring_url,
            keyring_sha256sum=actual_sha256,
        )

        responses.add(
            responses.GET, repository["keyring"]["url"], body=keyring_contents
        )

        self.task._deb822_source(
            repository, keyring_directory=keyring_directory, use_signed_by=True
        )

    @responses.activate
    def test_generate_deb822_sources_file(self):
        """Assert _generate_deb822_sources_file() return the expected files."""
        keyring_url = "https://example.com/keyring_1.gpg"
        responses.add(responses.GET, keyring_url)

        repository = {
            "types": ["deb"],
            "mirror": "https://deb.debian.org/deb",
            "suite": "bookworm",
            "components": ["main", "contrib"],
            "check_signature_with": "external",
        }

        for use_signed_by in [True, False]:
            with self.subTest(use_signed_by=use_signed_by):
                directory = self.create_temporary_directory()
                repository["keyring"] = {"url": keyring_url}

                sources = self.task._generate_deb822_sources(
                    [repository],
                    keyrings_dir=directory,
                    use_signed_by=use_signed_by,
                )

                self.assertEqual(sources[0]["URIs"], repository["mirror"])
                self.assertEqual(len(sources), 1)

                if use_signed_by:
                    self.assertEqual(
                        sources[0]["Signed-By"], str(next(directory.iterdir()))
                    )
                else:
                    self.assertNotIn("Signed-By", sources[0])
