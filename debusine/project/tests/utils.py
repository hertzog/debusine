# Copyright 2022 The Debusine developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Helper test functions."""

import importlib


def debusine_admin_main_importer():
    """Return main() from the installed /usr/bin/debusine-admin file."""
    spec = importlib.util.spec_from_loader(
        "manage",
        importlib.machinery.SourceFileLoader(
            "manage", "/usr/bin/debusine-admin"
        ),
    )
    manage = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(manage)
    return manage.main
